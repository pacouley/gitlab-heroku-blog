package org.gmslabs.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class WebApplicationTest extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplicationTest.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebApplicationTest.class, args);
    }
}
