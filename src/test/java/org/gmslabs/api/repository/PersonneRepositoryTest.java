package org.gmslabs.api.repository;

import org.gmslabs.api.config.H2TestProfileJPAConfig;
import org.gmslabs.api.model.Personne;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { H2TestProfileJPAConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Transactional
public class PersonneRepositoryTest {

    @Resource
    private PersonneRepository personneRepository;

    @Test
    public void testPersonne() {

        String email = "email";

        // Créer l'objet adresse
        Personne personne = new Personne();
        personne.setEmail(email);
        personneRepository.save(personne);

        // when
        Personne found = personneRepository.findByEmail(email);
        // then
        assertThat(found.getEmail()).isEqualTo(email);
    }
}
