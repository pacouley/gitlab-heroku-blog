package org.gmslabs.api.dto.personne;

import org.gmslabs.api.model.Personne;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonneMapper {
    PersonneMapper INSTANCE = Mappers.getMapper( PersonneMapper.class );
    PersonneDTO personneToDTO(Personne personne);
    Personne dtoToPersonne(PersonneDTO personneDTO);
    List<PersonneDTO> personneToListDTO(List<Personne> listPersonne);
}
