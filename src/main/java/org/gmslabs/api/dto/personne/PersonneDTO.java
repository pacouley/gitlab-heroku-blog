package org.gmslabs.api.dto.personne;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *
 * PersonneDTO
 *
 * @author pacouley
 */
@Data
@NoArgsConstructor
public class PersonneDTO{

    /**
     * email
     */
    private String email;

    /**
     * Login nom.prenom avec un chiffre pour différencier les doublons ou email
     *
     */
    private String login;

    /**
     * nom de la personne
     */
    private String nom;
}
