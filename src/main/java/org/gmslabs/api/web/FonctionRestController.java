package org.gmslabs.api.web;

import org.gmslabs.api.dto.fonction.FonctionDTO;
import org.gmslabs.api.dto.fonction.FonctionMapper;
import org.gmslabs.api.service.FonctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FonctionRestController extends BaseRestController {
    @Autowired
    private FonctionService fonctionService;

    @GetMapping("/fonctions/{code}")
    public FonctionDTO obtenirFonctionByCode(@PathVariable String code) {
        return FonctionMapper.INSTANCE.fonctionToDTO(fonctionService.obtenirFonctionByCode(code));
    }
}
